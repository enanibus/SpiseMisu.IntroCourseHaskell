#!/bin/bash

clear

./clear.bash

# names
ghc -Wall -Werror -O2 --make Main.hs -o names

# run
./names && echo

# clean
find . -name '*.hi' -delete
find . -name '*.o'  -delete
