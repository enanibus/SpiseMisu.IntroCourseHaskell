module JSON
  ( Value
    ( String
    , Number
    , Object
    , Array
    , Boolean
    , Null
    )
  ) where

--------------------------------------------------------------------------------

data Value
  = String  String
  | Number  Double
  | Object  [ (Label, Value) ]
  | Array   [ Value ]
  | Boolean Bool -- We can combine `true` and `false` into a single case
  | Null
type Label = String

--------------------------------------------------------------------------------

instance Show Value where
  show (String x)      = show x
  show (Number x)      = show x
  show (Object x)      =
    "{" ++ aux x ++ "}"
    where
      aux [] = ""
      aux xs =
        foldl1 (\ x y  ->      x ++ "," ++      y)
        $ map  (\(x,y) -> show x ++ ":" ++ show y) xs
  show (Array x)       =
    "[" ++ aux x ++ "]"
    where
      aux [] = ""
      aux xs =
        foldl1 (\x y -> x ++ "," ++ y)
        $ map show xs
  show (Boolean True)  = "true"
  show (Boolean False) = "false"
  show Null            = "null"
